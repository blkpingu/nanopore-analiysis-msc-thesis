import pandas as pd

def convert_parquet_to_csv(parquet_path):
    df = pd.read_parquet(parquet_path)
    print(df.head())

parquet_file_path_1 = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/024_barcode_08.parquet'
parquet_file_path_2 = '/Users/Tobias/Documents/Studium/Master_AI/Semester/Masterarbeit/Masterarbeit_Versuch_2/Code/Daten/part-00080-3b1cf26e-cf6e-41a6-b295-f9e7659913ec-c000.snappy.parquet'
convert_parquet_to_csv(parquet_file_path_2)
