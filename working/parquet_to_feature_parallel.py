from pyspark.sql import SparkSession
from pyspark.sql.functions import udf, col, pandas_udf, PandasUDFType
from pyspark.sql.types import FloatType, StructType, StructField, ArrayType, DoubleType
import numpy as np
from scipy import stats, signal
from scipy.signal import find_peaks
import os
import time
from datetime import datetime
from scipy.fftpack import fft
from pyspark.sql.window import Window
import pandas as pd
import torch
from read_autoencoder import ReadAutoencoder
import io
from pyspark.sql import functions as F
from pathlib import Path






schema = StructType([
    StructField("min", FloatType(), False),
    StructField("max", FloatType(), False),
    StructField("mean", FloatType(), False),
    StructField("median", FloatType(), False),
    StructField("std", FloatType(), False),
    StructField("var", FloatType(), False),
    StructField("aad", FloatType(), False),
    StructField("maxmin_diff", FloatType(), False),
    StructField("mad", FloatType(), False),
    StructField("iqr", FloatType(), False),
    StructField("neg_count", FloatType(), False),
    StructField("pos_count", FloatType(), False),
    StructField("above_mean", FloatType(), False),
    StructField("peak_count", FloatType(), False),
    StructField("skewness", FloatType(), False),
    StructField("kurtosis", FloatType(), False),
    StructField("energy", FloatType(), False),
    StructField("rms", FloatType(), False),
    StructField("fft_mag_std", FloatType(), False),
    StructField("fft_mag_skewness", FloatType(), False),
    StructField("fft_mag_kurtosis", FloatType(), False),
    StructField("fft_phase_std", FloatType(), False),
    StructField("fft_phase_skewness", FloatType(), False),
    StructField("fft_phase_kurtosis", FloatType(), False)
])

# TODO: include labels from raw data
# TODO: add data_read_features["length"] = float(len(raw))



def compute_features(array):
    """
    Computes statistical features for the provided array.

    :param array: List of numbers for which features are computed.
    :return: Tuple containing computed features.
    """
    array_np = np.array(array)

    fft_vals = fft(array_np)
    fft_magnitude = np.abs(fft_vals)
    fft_phase = np.angle(fft_vals)

    return (
        float(np.min(array_np)),
        float(np.max(array_np)),
        float(np.mean(array_np)),
        float(np.median(array_np)),
        float(np.std(array_np)),
        float(np.var(array_np)),
        float(np.mean(np.absolute(array_np - np.mean(array_np)))),
        float(np.max(array_np) - np.min(array_np)),
        float(np.median(np.absolute(array_np - np.median(array_np)))),
        float(np.percentile(array_np, 75) - np.percentile(array_np, 25)),
        float(np.sum(array_np < 0)),
        float(np.sum(array_np > 0)),
        float(np.sum(array_np > np.mean(array_np))),
        float(len(find_peaks(array_np)[0])),
        float(stats.skew(array_np)),
        float(stats.kurtosis(array_np)),
        float(np.sum(array_np ** 2) / 100),
        float(np.sqrt(np.mean(np.mean(array_np ** 2) ** 2)) if np.mean(array_np ** 2) >= 0 else -1.0),
        float(np.std(fft_magnitude)),
        float(stats.skew(fft_magnitude)),
        float(stats.kurtosis(fft_magnitude)),
        float(np.std(fft_phase)),
        float(stats.skew(fft_phase)),
        float(stats.kurtosis(fft_phase))
    )

feature_udf = udf(compute_features, schema)

def format_duration(seconds):
    """
    Formats the given duration in seconds to DD-HH-MM-SS format.

    :param seconds: Duration in seconds.
    :return: Formatted string.
    """
    days, remainder = divmod(seconds, 86400)
    hours, remainder = divmod(remainder, 3600)
    minutes, seconds = divmod(remainder, 60)
    return f"{int(days):02d}-{int(hours):02d}-{int(minutes):02d}-{int(seconds):02d}"

def finalize_output_directory(start_time, out_dir, final_dir_prefix, start_datetime):
    end_time = time.time()
    end_datetime = datetime.now()
    duration_str = format_duration(end_time - start_time)
    logfile = os.path.join(out_dir, f"duration_{start_datetime.strftime('%Y_%m_%d_%H-%M-%S')}.txt")
    with open(logfile, "a") as f:
        f.write(f"Start DateTime: {start_datetime.strftime('%Y-%m-%d %H:%M:%S')}\n")
        f.write(f"End DateTime: {end_datetime.strftime('%Y-%m-%d %H:%M:%S')}\n")
        f.write(f"Duration: {duration_str}\n")
        f.write(f"Target Directory: {out_dir}\n")


def compute_read_features(root_dir, spark):
    """
    Processes all parquet files in the specified directory using Spark's built-in parallelism.

    :param directory: Path to the directory containing the parquet files.
    """
    start_time = time.time()
    start_datetime = datetime.now()

    df = spark.read.parquet(os.path.join(root_dir, "*.parquet"))

    df_with_features = df.withColumn("features", feature_udf(col("raw_data")))
    for feature_name in schema.fieldNames():
        df_with_features = df_with_features.withColumn(feature_name, col("features." + feature_name))
    df_with_features = df_with_features.drop("features")

    out_dir = os.path.join(output_dir, f'outdir_chunk-{start_time}')
    df_with_features.write.parquet(out_dir)

    finalize_output_directory(start_time, out_dir, "parallel_stats", start_datetime)
    return out_dir


def compute_chunk_features(output_dir, spark, source_dir, chunk_size):
    """
    Computes chunk features for each column in the dataset.

    :param output_dir: Directory containing the original parquet files.
    :param chunk_size: The size of each chunk to process.
    :param schema: The schema of the original dataset.
    """

    start_time = time.time()
    start_datetime = datetime.now()

    # Read the data

    print(f'ToChunk: {source_dir}')
    df = spark.read.parquet(os.path.join(source_dir, "*.parquet"))

    # Define window for chunk processing
    windowSpec = Window.partitionBy("label").orderBy("read_id").rowsBetween(0, chunk_size - 1)


    # TODO: incomplete chunks
    # TODO: group by labels
    # TODO: shuffle grouped labels

    # TODO: incomplete chunks handling / gerne auch rauswerfen
    # if include_incomplete:
    # remaining = len(group) % chunk_size
    # if remaining > 0:
    #     chunk = group.iloc[-remaining:]
    #     chunks[label].append(chunk.drop(columns=[group_by]))

    # Compute chunk features for each original feature
    for field in schema.fields:
        feature_name = field.name
        df = df.withColumn(f"{feature_name}_std", F.stddev(col(feature_name)).over(windowSpec))
        df = df.withColumn(f"{feature_name}_mean", F.mean(col(feature_name)).over(windowSpec))
        df = df.withColumn(f"{feature_name}_skewness", F.skewness(col(feature_name)).over(windowSpec))
        df = df.withColumn(f"{feature_name}_kurtosis", F.kurtosis(col(feature_name)).over(windowSpec))

    out_dir = os.path.join(output_dir, f'outdir_chunk-{start_time}')
    df.write.parquet(out_dir)

    # TODO:return features.feather
    finalize_output_directory(start_time, out_dir, "chunk_features", start_datetime)



# def compute_autoencoder_features(root_dir):
#     start_time = time.time()
#     start_datetime = datetime.now()
#     df_source = spark.read.parquet(os.path.join(root_dir, "source", "*.parquet"))

#     def run_autoencoder(model, data):
#         with torch.no_grad():
#             output = model(data)
#             return output.numpy()



#     def preprocess_for_autoencoder(data_series, input_dim=8192):
#         numpy_data = np.array(data_series.tolist())
#         tensor_data = torch.tensor(numpy_data).type(torch.float32).reshape(-1, 1, input_dim)
#         return tensor_data

#     def load_and_serialize_model(checkpoint_path, bottleneck_dim):
#         # Use PyTorch Lightning's method to load the model from the checkpoint
#         model = ReadAutoencoder.load_from_checkpoint(
#             checkpoint_path,
#             bottleneck_dim=bottleneck_dim
#         )
#         # Set the model to evaluation mode
#         model.eval()

#         byte_stream = io.BytesIO()
#         torch.save(model, byte_stream)
#         return byte_stream.getvalue()

#     broadcasted_models = {}
#     for dim, path in autoencoder_configs:
#         serialized_model = load_and_serialize_model(path, dim)
#         broadcasted_models[dim] = spark.sparkContext.broadcast(serialized_model)

#     def preprocess_raw_data_udf(array, target_length=8192):
#         if array is None:
#             return [0.0] * target_length
#         current_length = len(array)
#         if current_length > target_length:
#             return array[:target_length]
#         elif current_length < target_length:
#             padding = [0.0] * (target_length - current_length)
#             return array + padding
#         else:
#             return array

#     resize_udf = udf(preprocess_raw_data_udf, ArrayType(FloatType()))

#     df_with_resized_column = df_source.withColumn("raw_data", resize_udf("raw_data"))

#     output_fields = []
#     for dim, _ in autoencoder_configs:
#         for i in range(dim):
#             output_fields.append(StructField(f'feature_{len(output_fields)}', DoubleType()))

#     udf_return_type = StructType(output_fields)

#     @pandas_udf(udf_return_type, PandasUDFType.SCALAR)
#     def autoencoder_udf(raw_data_series):
#         results = []

#         for dim, _ in autoencoder_configs:
#             serialized_model = broadcasted_models[dim].value
#             byte_stream = io.BytesIO(serialized_model)
#             model = torch.load(byte_stream)

#             tensor_data = preprocess_for_autoencoder(raw_data_series, input_dim=8192)
#             output = run_autoencoder(model, tensor_data)
#             results.append(output)

#         return pd.concat(results, axis=1)

#     sauce = df_source.select("raw_data").first()[0]
#     print(sauce)
#     print(len(sauce))
#     sample = df_with_resized_column.select("raw_data").first()[0]
#     print(sample)
#     print(len(sample))


#     df_with_autoencoder_features = df_with_resized_column.withColumn('autoencoder_features', autoencoder_udf("raw_data"))
#     output_dir = os.path.join(root_dir, "autoencoder_features")
#     df_with_autoencoder_features.write.parquet(output_dir)

#     finalize_output_directory(start_time, output_dir, "autoencoder_features", start_datetime)

def create_spark(cores='*', executor_memory=300, driver_memory=300, executor_memory_overhead=8, driver_memory_overhead=8):

    spark = SparkSession.builder \
        .appName("Parquet Feature Extraction") \
        .master(f'local[{cores}]') \
        .config("spark.driver.memory", f'{driver_memory}g') \
        .config("spark.executor.memory", f'{executor_memory}g') \
        .config("spark.driver.memoryOverhead", f'{driver_memory_overhead}g') \
        .config("spark.executor.memoryOverhead", f'{executor_memory_overhead}g') \
        .getOrCreate()

    conf = spark.sparkContext.getConf()

    logger = spark.sparkContext._jvm.org.apache.log4j
    logger.LogManager.getLogger("org").setLevel(logger.Level.ERROR)

    app_name = conf.get("spark.app.name")
    spark_master = conf.get("spark.master")

    print(f"App Name: {app_name}")
    print(f"Spark Master: {spark_master}")

    return spark

def test_runner(test=None, source_dir=None, spark=None, output_dir=None, chunk_size=None):

    # stat_features_dir = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/parquet_test/parallel_stats_00-00-05-33'
    start_time = time.time()

    stat_features_dir = compute_read_features(source_dir, spark)
    compute_chunk_features(output_dir, spark, stat_features_dir, test[4])

    end_time = time.time()

    return end_time - start_time

def test(source_dir, output_dir):

    print(f'Begin processing: {source_dir}')
    print(f'Output directory: {output_dir}')

    # Parameters for generating test cases

    # # Planned tests
    # core_count = [10, 20, 40, 64]
    # memory = [50, 100, 300]
    # memory_overhead = [8]
    # chunk_size = [100, 500, 1000, 2000, 4000, 8000]

    # # finished tests
    # core_count = [10]
    # memory = [50, 100, 300]
    # memory_overhead = [8]
    # chunk_size = [100, 500, 1000, 2000, 4000, 8000]

    # finished tests
    # core_count = [64]
    # memory = [50, 100, 300]
    # memory_overhead = [8]
    # chunk_size = [100, 500, 1000, 2000, 4000, 8000]

    core_count = [64]
    memory = [300]
    memory_overhead = [8]
    chunk_size = [500]

    # core_count = [64]
    # memory = [300]
    # memory_overhead = [8]
    # chunk_size = [10]
    # Generating test cases

    test_blocks=[]
    for core in core_count:
        test_cases = []
        for mem in memory:
            for overhead in memory_overhead:
                for chunk in chunk_size:
                    test_name = f"test-C{core}-M{mem}GB-O{overhead}-CS{chunk}"
                    test_cases.append((test_name, core, mem, overhead, chunk))
        test_blocks.append(test_cases)


    for index,block in enumerate(test_blocks):
        print(f'Block {index+1} of {len(test_blocks)}')
        results=[]
        for index, test in enumerate(block):

            print(f'{test[0]}: {index+1} of {len(block)}')

            spark = create_spark(cores=test[1], executor_memory=test[2], driver_memory=test[2], executor_memory_overhead=test[3], driver_memory_overhead=test[3])
            # spark = create_spark()
            test_time = test_runner(test=test, source_dir=source_dir, spark=spark, output_dir=output_dir, chunk_size=test[4])

            results.append((test[0], test[1], test[2], test[3], test[4], test_time))

            test_times_df = pd.DataFrame(results, columns=['test_name','core_count', 'memory', 'memory_overhead', 'chunk_size', 'time'])
            test_times_df.to_csv(f'/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/barcode_to_parquet_parallel/core_{test[1]}_results.csv', index=False)


if __name__ == "__main__":

    source_dir = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/parquet_test'
    output_dir = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/parquet_to_feature_parallel'


    test(source_dir=source_dir, output_dir=output_dir)

    # spark = SparkSession.builder \
    # .appName("Parquet Feature Extraction") \
    # .master("local[*]") \
    # .config("spark.driver.memory", "300g") \
    # .config("spark.executor.memory", "300g") \
    # .config("spark.driver.memoryOverhead", "8g") \
    # .config("spark.executor.memoryOverhead", "8g") \
    # .getOrCreate()

    # conf = spark.sparkContext.getConf()

    # logger = spark.sparkContext._jvm.org.apache.log4j
    # logger.LogManager.getLogger("org").setLevel(logger.Level.ERROR)


    # app_name = conf.get("spark.app.name")
    # spark_master = conf.get("spark.master")

    # print(f"App Name: {app_name}")
    # print(f"Spark Master: {spark_master}")

    # autoencoder_configs = [
    #     (2, "./checkpoints/read_autoencoder_2d/epoch=9-step=1765000.ckpt"),
    #     (3, "./checkpoints/read_autoencoder_3d/epoch=9-step=1765000.ckpt"),
    #     (8, "./checkpoints/read_autoencoder_8d/epoch=9-step=1765000.ckpt"),
    #     (16, "./checkpoints/read_autoencoder_16d/epoch=9-step=1765000.ckpt"),
    #     (32, "./checkpoints/read_autoencoder_32d/epoch=9-step=1765000.ckpt"),
    # ]




    # chunk_size = 100
    # root_dir = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/complete/'

    # source_dir = compute_read_features(root_dir)
    # # autoencoder_source_dir = compute_autoencoder_features(root_dir)
    # # add autoencoder featrues to compute chunk features
    # compute_chunk_features(root_dir, autoencoder_source_dir, chunk_size, schema)