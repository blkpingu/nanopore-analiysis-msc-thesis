import pytorch_lightning as pl
import torch.nn.functional as F
from pytorch_lightning.utilities import grad_norm
from torch import nn, optim


class ReadAutoencoder(pl.LightningModule):
    # bottleneck_dim max is 256
    def __init__(
        self,
        bottleneck_dim=3,
        hidden_dim=32,
        input_dim=8192,
    ):
        super().__init__()

        self.learning_rate = 1e-3
        self.training_dataloader = None
        self.validation_dataloader = None

        self.encoder = nn.Sequential(
            nn.Unflatten(dim=1, unflattened_size=(1, input_dim)),
            nn.Conv1d(
                in_channels=1,
                out_channels=hidden_dim,
                kernel_size=3,
                stride=2,
                padding=1,
            ),
            nn.LeakyReLU(),
            nn.Conv1d(
                in_channels=hidden_dim,
                out_channels=hidden_dim,
                kernel_size=3,
                stride=2,
                padding=1,
            ),
            nn.LeakyReLU(),
            nn.Conv1d(
                in_channels=hidden_dim,
                out_channels=1,
                kernel_size=3,
                stride=2,
                padding=1,
            ),
            nn.LeakyReLU(),
            nn.Flatten(),
            nn.Linear(input_dim // 8, bottleneck_dim),
            nn.LeakyReLU(),
        )

        self.decoder = nn.Sequential(
            nn.Linear(bottleneck_dim, input_dim // 8),
            nn.LeakyReLU(),
            nn.Unflatten(dim=1, unflattened_size=(1, input_dim // 8)),
            nn.ConvTranspose1d(
                in_channels=1,
                out_channels=hidden_dim,
                kernel_size=3,
                stride=2,
                padding=1,
                output_padding=1,
            ),
            nn.LeakyReLU(),
            nn.ConvTranspose1d(
                in_channels=hidden_dim,
                out_channels=hidden_dim,
                kernel_size=3,
                stride=2,
                padding=1,
                output_padding=1,
            ),
            nn.LeakyReLU(),
            nn.ConvTranspose1d(
                in_channels=hidden_dim,
                out_channels=1,
                kernel_size=3,
                stride=2,
                padding=1,
                output_padding=1,
            ),
            nn.Sigmoid(),
            nn.Flatten(),
        )

    def set_learning_rate(self, learning_rate):
        self.learning_rate = learning_rate

    def set_training_dataloader(self, training_dataloader):
        self.training_dataloader = training_dataloader

    def set_validation_dataloader(self, validation_dataloader):
        self.validation_dataloader = validation_dataloader

    def train_dataloader(self):
        return self.training_dataloader

    def val_dataloader(self):
        return self.validation_dataloader

    def forward(self, x):
        z = self.encoder(x)
        x_hat = self.decoder(z)

        return x_hat

    def training_step(self, batch, batch_idx):
        self.train()

        x, _ = batch
        x_hat = self(x)

        loss = F.mse_loss(x_hat, x)
        self.log("train_loss", loss)

        return loss

    def validation_step(self, batch, batch_idx):
        self.eval()

        x, _ = batch
        x_hat = self(x)

        val_loss = F.mse_loss(x_hat, x)
        self.log("val_loss", val_loss)

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=self.learning_rate)
        return optimizer