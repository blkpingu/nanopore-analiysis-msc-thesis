from ont_fast5_api.fast5_interface import get_fast5_file
import numpy as np
import h5py
import pandas as pd
import os
import faulthandler
import glob
import gc
from pathlib import Path
import time
from datetime import datetime



def raw_data_to_numpy_array(readfile):
    data = []

    fast5_filepath = readfile
    with get_fast5_file(fast5_filepath, mode="r") as f5:
        for read in f5.get_reads():
            raw_data = read.get_raw_data()
            tup = read.read_id, raw_data
            data.append(tup)

    return pd.DataFrame(data, columns=['read_id', 'raw_data'])


def get_paths(source_dir, iter):
    barcode_iter = 'barcode' + iter
    barcode_pass = os.path.join(source_dir, 'fast5_pass', barcode_iter)
    barcode_fail = os.path.join(source_dir, 'fast5_fail', barcode_iter)

    paths = []
    if os.path.exists(barcode_pass):
        paths.append(barcode_pass)
    if os.path.exists(barcode_fail):
        paths.append(barcode_fail)

    return paths if paths else None


def walk_through_files(path, file_extension='.fast5'):
    return glob.glob(os.path.join(path, "*" + file_extension))

def get_label(label_path, current_fast5):
    df = pd.read_csv(label_path)

    if current_fast5 in df['path'].values:
        return df.loc[df['path'] == current_fast5, 'label'].iloc[0]
    else:
        return None

    # TODO: kein label, keine aufnahme in den datensatz


def add_label_column(barcode_df, result_value):

    if result_value is None:
        result_value = 0

    barcode_df['label'] = result_value
    return barcode_df

def combine_files(source_dir, iterator, label_path):
    barcode_list = get_paths(source_dir, iterator)
    if barcode_list is None:
        return None
    barcode_parts = []
    for barcode in barcode_list:
        for fast5_path in walk_through_files(barcode):
            barcode_part = raw_data_to_numpy_array(fast5_path)
            barcode_part = add_label_column(barcode_part, get_label(Path(label_path), fast5_path))
            barcode_parts.append(barcode_part)

    return pd.concat(barcode_parts)

def iter_run(run_path, run_id, output_dir, label_path, out_format=None):

    for iterator in ["{0:02}".format(i) for i in range(1,100)]:
        df = combine_files(run_path, iterator, label_path)
        if df is None:
            continue
        if out_format == 'feather':
            output_file_path = os.path.join(output_dir, run_id + '_barcode_' + iterator + '.feather')
            df.to_feather(output_file_path, compression='uncompressed', compression_level=None, chunksize=None, version=2)
        elif out_format == 'parquet':
            output_file_path = os.path.join(output_dir, run_id + '_barcode_' + iterator + '.parquet')
            df.to_parquet(output_file_path, engine='pyarrow', compression='snappy')
        elif out_format == None:
            output_file_path = os.path.join(output_dir, run_id + '_barcode_' + iterator + '.csv')
            df.to_csv(output_file_path, index=False)
        del df
        gc.collect()

def run_all(source_dir, run_id_list, output_dir, label_path, out_format=None):
    dirs = os.listdir(source_dir)
    for run_id in run_id_list:
        run_folder_name = [d for d in dirs if d.endswith(run_id)]
        if run_folder_name:
            run_path = os.path.join(source_dir, run_folder_name[0])
            iter_run(run_path, run_id, output_dir, label_path, out_format=out_format)

def run(source_dir, run_id_list, output_dir, label_path, name_id=None, out_format=None):

    print(f"Begin processing: {source_dir}")
    start_time = time.time()
    output_dir = Path(output_dir) / f'{datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}-{name_id}'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    faulthandler.enable()
    run_all(source_dir=source_dir, run_id_list=run_id_list, output_dir=output_dir, label_path=label_path, out_format=out_format)

    end_time = time.time()

    print(f"End processing: {source_dir}")
    print(f"Total processing time: {end_time - start_time} seconds")

    # Write the processing time to a text file
    with open(Path(output_dir) / "processing_time.txt", "w") as file:
        file.write(f"Total processing time: {end_time - start_time} seconds")

    return end_time - start_time

if __name__ == '__main__':

    run_id_list = ['024', '084', '085', '086', '107', '123', '135']
    test_path = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/raw_test/unpacked_small'
    source_dir = '/data/ldap/pdabrowski/CovidSpiNGS/RawData/RKI/MinION_fast5/unpacked'
    output_dir = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/barcode_to_parquet'
    label_path = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/ahinzer/concentration_prediction/rki_fast5_register_ct.csv'

    run(test_path, run_id_list, output_dir, label_path, name_id='test-parquet', out_format='parquet')