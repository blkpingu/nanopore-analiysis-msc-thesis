import os
import pandas as pd
import numpy as np
from scipy import stats
from scipy.signal import find_peaks
import time
import progressbar

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# Define the statistical data points to be calculated
raw_features = {
    "min": lambda x: np.min(x),
    "max": lambda x: np.max(x),
    "mean": lambda x: np.mean(x),
    "median": lambda x: np.median(x),
    "std": lambda x: np.std(x),
    "var": lambda x: np.var(x),
    "aad": lambda x: np.mean(np.absolute(x - np.mean(x))),
    "maxmin_diff": lambda x: np.max(x) - np.min(x),
    "mad": lambda x: np.median(np.absolute(x - np.median(x))),
    "iqr": lambda x: np.percentile(x, 75) - np.percentile(x, 25),
    "neg_count": lambda x: float(np.sum(x < 0)),
    "pos_count": lambda x: float(np.sum(x > 0)),
    "above_mean": lambda x: float(np.sum(x > np.mean(x))),
    "peak_count": lambda x: float(len(find_peaks(x)[0])),
    "skewness": lambda x: stats.skew(x),
    "kurtosis": lambda x: stats.kurtosis(x),
    "energy": lambda x: np.sum(x ** 2) / 100,
    "rms": lambda x: np.sqrt(np.mean(np.mean(x ** 2) ** 2)) if np.mean(x ** 2) >= 0 else -1.0,
}

def compute_features(df, array_column):
    """
    Computes statistical features for the specified DataFrame column.
    """
    for feature_name, function in raw_features.items():
        df[feature_name] = df[array_column].apply(lambda x: function(np.array(x)))
    return df

def format_duration(seconds):
    """
    Formats the given duration in seconds to DD-HH-MM-SS format.
    """
    days, remainder = divmod(seconds, 86400)
    hours, remainder = divmod(remainder, 3600)
    minutes, seconds = divmod(remainder, 60)
    return f"{int(days):02d}-{int(hours):02d}-{int(minutes):02d}-{int(seconds):02d}"

def process_parquet_files(directory):
    """
    Processes parquet files in the specified directory, computing statistical features.
    """
    start_time = time.time()

    output_dir_name = "with_stats"
    output_dir = os.path.join(directory, output_dir_name)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    files = [f for f in os.listdir(directory) if f.endswith('.parquet')]
    for file in files:
        df = pd.read_parquet(os.path.join(directory, file))
        df = compute_features(df, "raw_data")  # Updated column name here
        new_file_name = os.path.splitext(file)[0] + "_s.parquet"
        df.to_parquet(os.path.join(output_dir, new_file_name))

    end_time = time.time()
    return end_time - start_time

def process_with_progress_bar(directory):
    """
    Processes parquet files with a progress bar.
    """
    files = [f for f in os.listdir(directory) if f.endswith('.parquet')]
    bar = progressbar.ProgressBar(maxval=len(files)).start()  # Modified this line
    duration = process_parquet_files(directory)

    for index in range(len(files)):
        bar.update(index + 1)

    bar.finish()

    duration_str = format_duration(duration)
    new_output_dir = os.path.join(directory, f"with_stats_{duration_str}")
    os.rename(os.path.join(directory, "with_stats"), new_output_dir)


if __name__ == "__main__":
    source_dir = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/complete/'
    process_with_progress_bar(source_dir)
