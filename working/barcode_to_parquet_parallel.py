from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, ArrayType, IntegerType
from ont_fast5_api.fast5_interface import get_fast5_file
from pathlib import Path
import time
from datetime import datetime
import os
import uuid
import numpy as np
import pandas as pd


def process_and_write_batches(spark, source_dir, label_path, output_dir, records_per_file, batch_size, out_format=None):
    file_paths = [str(file) for file in Path(source_dir).rglob('*.fast5')]

    schema = StructType([
        StructField("path", StringType(), True),
        StructField("read_id", StringType(), True),
        StructField("raw_data", ArrayType(IntegerType()), True)
    ])

    labels_df = read_labels(spark, label_path)

    for batch_data in process_multiple_fast5_files_in_batches(file_paths, batch_size):
        batch_df = spark.createDataFrame(batch_data, schema=schema)
        joined = batch_df.join(labels_df, batch_df.path == labels_df.path, "left_outer").drop("path")


        if joined is None:
            continue
        if out_format == 'parquet':
            output_file_path = output_dir / f"{uuid.uuid4()}.parquet"
            joined.coalesce(100).write.option("maxRecordsPerFile", records_per_file).mode("append").parquet(str(output_file_path))
        elif out_format == None:
            output_file_path = output_dir / f"{uuid.uuid4()}.csv"
            joined.coalesce(100).write.option("maxRecordsPerFile", records_per_file).mode("append").csv(str(output_file_path))


def read_labels(spark, label_path):
    return spark.read.csv(label_path, header=True, inferSchema=True)


def process_multiple_fast5_files_in_batches(file_paths, batch_size):
    batch_data = []
    current_batch_size = 0

    total_files = len(file_paths)
    for index, file_path in enumerate(file_paths):
        print(f"Processing file: {index +1}/{total_files}")
        with get_fast5_file(file_path, mode="r") as f5:
            for read in f5.get_reads():
                raw_data_list = read.get_raw_data().tolist()
                batch_data.append((file_path, read.read_id, raw_data_list))
                current_batch_size += 1

                if current_batch_size >= batch_size:
                    yield batch_data
                    batch_data = []
                    current_batch_size = 0

        if batch_data:
            yield batch_data




def run(source_dir, label_path, output_dir, records_per_file=5000, batch_size=1000, name_id=None, out_format=None):

    spark = SparkSession.builder \
        .appName("Label_Fast5_Files") \
        .master("local[*]") \
        .config("spark.driver.memory", "300g") \
        .config("spark.executor.memory", "300g") \
        .config("spark.driver.memoryOverhead", "8g") \
        .config("spark.executor.memoryOverhead", "8g") \
        .getOrCreate()

    print(f"Begin processing: {source_dir}")
    start_time = time.time()
    output_dir = Path(output_dir) / f'{datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}-{name_id}'
    output_dir.mkdir(parents=True, exist_ok=True)

    process_and_write_batches(spark, source_dir, label_path, output_dir, records_per_file, batch_size, out_format=out_format)

    end_time = time.time()
    print(f"End processing: {source_dir}")
    print(f"Total processing time: {end_time - start_time} seconds")

    with open(output_dir / "processing_time.txt", "w") as file:
        file.write(f"Total processing time: {end_time - start_time} seconds")

    return end_time - start_time


def test(source_dir, label_path, output_dir, tests_per_type=3):

    # Parameters for testing
    file_formats = ['parquet', 'csv']
    records_per_file_options = [1000, 3000, 5000, 10000, 50000]
    batch_size_options = [500, 1000, 1500, 2000, 2500, 3000]

    # Generating test tuples
    tests = []
    for ff in file_formats:
        for records in records_per_file_options:
            for batch_size in batch_size_options:
                test_name = f"test-{ff}-R{records}-B{batch_size}"
                tests.append((test_name, ff, records, batch_size))

    # Selecting a subset of tests to limit to 20 per file format
    for ff in file_formats:
        selected_tests = []
        format_tests = [test for test in tests if test[1] == ff]
        # selected_tests.extend(format_tests[:tests_per_type])  # 45 = 1000, 5000, 10000, 20000, 30000
        selected_tests.extend(format_tests)  # 45 = 1000, 5000, 10000, 20000, 30000

        results = []

        for index, test in enumerate(selected_tests):
            print(f'{ff}-Test {index+1} of {len(selected_tests)}')
            time_result = run(test_path, label_path, output_dir, records_per_file=test[2], batch_size=test[3], name_id=test[0], out_format=test[1])
            results.append((test[2], test[3], time_result))

        test_times_df = pd.DataFrame(results, columns=['records_per_file','batch_size', 'Time'])
        test_times_df.to_csv(f'/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/barcode_to_parquet_parallel/{ff}_results.csv', index=False)


if __name__ == '__main__':
    source_dir = '/data/ldap/pdabrowski/CovidSpiNGS/RawData/RKI/MinION_fast5/unpacked'
    label_path = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/ahinzer/concentration_prediction/rki_fast5_register_ct.csv'
    output_dir = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/barcode_to_parquet_parallel'
    test_path = '/data/ldap/pdabrowski/CovidSpiNGS/ProcessedData/tkolb/raw_test/unpacked_small'


    # run(test_path, label_path, output_dir, records_per_file=10000, batch_size=2500, name_id='test-parquet', out_format='parquet')
    test(test_path, label_path, output_dir, tests_per_type=2)